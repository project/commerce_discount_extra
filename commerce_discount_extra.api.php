<?php
/**
 * @file
 * Hooks provided by the Commerce Discount Extra module.
 */


/**
 * Allows modules to alter the manifest used to calculated per-quantity discount
 * offers determined by product IDs.
 *
 * The manifest is an array of line item wrappers representing products on an
 * order with each product line item resulting in one item in the manifest array
 * per quantity of the line item. Thus, one line item on an order may result in
 * multiple items in the manifest array.
 *
 * To determine if a per-quantity discount (a.k.a. BOGO) should apply to an
 * order, the discount module iterates over the manifest looking first for the
 * required number of trigger products and then looking for any potential offer
 * products that should result in an order being discounted.
 *
 * This hook allows modules to remove items from the manifest based on their own
 * conditions prior to the manifest being processed, essentially letting a site
 * developer disqualify a product from being a trigger product or offer product
 * based on some condition that isn't accounted for in the default UI.
 */
function hook_commerce_discount_per_quantity_manifest_alter(&$manifest) {
  // Exclude product 3 from the manifest (for whatever reason).
  foreach ($manifest as $key => $line_item_wrapper) {
    if ($line_item_wrapper->commerce_product->raw() == 3) {
      unset($manifest[$key]);
    }
  }
}

/**
 * Allows modules to alter the manifest used to calculated per-quantity discount
 * offers determined by product IDs.
 *
 * The manifest is an array of line item wrappers representing products on an
 * order with each product line item resulting in one item in the manifest array
 * per quantity of the line item. Thus, one line item on an order may result in
 * multiple items in the manifest array.
 *
 * To determine if a per-quantity category discount (a.k.a. BOGO from specified
 * product categories) should apply to an order, the discount module iterates
 * over the manifest looking first for the required number of products in
 * trigger categories and then looking for any products in offer categories that
 * should result in an order being discounted.
 *
 * This hook allows modules to remove items from the manifest based on their own
 * conditions prior to the manifest being processed, essentially letting a site
 * developer disqualify a product from being a trigger product or offer product
 * based on some condition that isn't accounted for in the default UI.
 */
function hook_commerce_discount_per_quantity_category_manifest_alter(&$manifest) {
  // Exclude product 3 from the manifest (for whatever reason).
  foreach ($manifest as $key => $line_item_wrapper) {
    if ($line_item_wrapper->commerce_product->raw() == 3) {
      unset($manifest[$key]);
    }
  }
}

/**
 * Allows modules to react the the save of a discount line item created by one
 * of the "Per quantity" offers.
 *
 * The $discount_line_item parameter is an object, so it gets passed by
 * reference to any hook implementation. Implementations do not need to save the
 * line item if they change it. Instead, the line item is compared to its
 * unmodified state after the hook is invoked to see if it changed. If so, the
 * calling function will save the updated line item.
 *
 * @param $discount_line_item
 *   The discount line item that was updated or created for the discount.
 * @param $discount_name
 *   The name of the discount that resulted in the discount line item.
 * @param $offer_type
 *   The machine-name of the "Per quantity" offer type on the discount.
 */
function hook_commerce_discount_per_quantity_discount_line_item_save($discount_line_item, $discount_name, $offer_type) {
  // No example.
}
